﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Destroyable : MonoBehaviour{
    [SerializeField]
 	protected int m_hitPoints;
	protected int m_currentHitpoints;
	protected bool m_invunarable = false;

	virtual public void Damage(int damage)
	{
      if(!m_invunarable)
	    {
		   m_currentHitpoints -= damage;
			 if(m_currentHitpoints <= 0)
			 {
				 m_Respawn();
			 }
	    }
	}
	protected virtual void m_Respawn()
	{
        Destroy(this.gameObject);
	}
	virtual protected void m_Restore()
	{
      m_currentHitpoints = m_hitPoints;
	}
	void Awake()
	{
	  m_currentHitpoints = m_hitPoints;
	}
}
