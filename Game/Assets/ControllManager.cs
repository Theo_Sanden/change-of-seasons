﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllManager : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		GameObject[] player = GameObject.FindGameObjectsWithTag("Player");

		for(int i = 0; i < player.Length; i++)
		{
		  player[i].GetComponent<PlayerScript>().SetGamePad(i);
          
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
