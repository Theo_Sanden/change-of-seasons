﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : Destroyable {
	public Transform targetToSnapTo;
	public float offsetFromTarget;
	public float speed;
	private float movement;
	public float rotationSpeed;
	private Rigidbody rig;
	private Vector3 forward,right;
	public GameObject camera;
	public Vector3 cameraoffset;
	public bool disabled;
	public GameObject spawnPoint;
	[SerializeField]
	string bulletID;

	[SerializeField]
	float fireDelay,playerSpawnCooldown;
	[SerializeField]
	int bulletDamage;
	float fireTimer,deathTimer;
	bool activeFire,justActivatedFire;
	[SerializeField]
	GameObject bulletNozzle;
	string Horizontal,Vertical,Shoot,Rotate;
	bool dash;
	[SerializeField]
	float dashCooldown,dashMaxSpeed, dashLengthTime, dashWindUpDown, cameraFOVStart,cameraFOVMax;
	float dashCooldownTimer, dashState = 1;
	float dashStartValue = 0,dashLerpValue,dashLerpStep, cameraLerpValue, dashTimer;

	[SerializeField]
	int[] validTargets;
	void Start () {
		 transform.position = spawnPoint.transform.position;
		 transform.up = transform.position - targetToSnapTo.transform.position;
		 camera.transform.position = transform.position + (cameraoffset.x * transform.forward) + (cameraoffset.y * transform.up);
		 camera.transform.LookAt(transform.position + new Vector3(0,cameraoffset.z,0), transform.up);
		 camera.GetComponent<Camera>().fieldOfView = cameraFOVStart;
		 bulletNozzle.transform.position += transform.localScale.x * transform.forward;	 
	}
	void Update () {
		//print(Input.GetAxis(Horizontal));
		if(!disabled){
		if(!dash && dashCooldownTimer == 0 && Input.GetAxis(Shoot) < 0)
		{
            dash = true;
			dashCooldownTimer = dashCooldown;
		}
		if(!dash)
		{
        transform.RotateAround(targetToSnapTo.transform.position, transform.right, speed * Time.deltaTime * Input.GetAxis(Vertical)); //Propels the player forward
	    transform.RotateAround(targetToSnapTo.transform.position, transform.forward, speed * Time.deltaTime * Input.GetAxis(Horizontal));//Strafes to the sides
	    transform.RotateAround(transform.position,transform.up, Time.deltaTime* rotationSpeed * Input.GetAxis(Rotate));//Rotates the forward axis around it's up axis
		}
		if(dash)
		{
         if(dashStartValue == 0)
		 {
			 dashStartValue = speed * Input.GetAxis(Vertical) * Time.deltaTime;
		 }
		   dash = lerpFromTo();
		 if(!dash)
		 {
		  dashState = 1;
          dashStartValue = 0;
		 }
		}
	   activeFire = (Input.GetAxis(Shoot) > 0)? true: false;
	   if(fireTimer == 0)
	   {
           if(activeFire)
		   {
			  justActivatedFire = true;
			  Fire(bulletID);
		   }
	   }
	   if(activeFire || (!activeFire && fireTimer < fireDelay && justActivatedFire == true))
	   {
		   fireTimer += Time.deltaTime;
	   }
	   if(fireTimer >= fireDelay){fireTimer = 0; justActivatedFire = false;}
	   if(dashCooldownTimer <0)
	   {
		   dashCooldownTimer = 0;
	   }
	   else if(dashCooldownTimer != 0){dashCooldownTimer -= Time.deltaTime;	}
	   if(camera.GetComponent<Camera>().fieldOfView != cameraFOVStart){CameraLerpToStart();}
	  }
	  if(disabled)
	  {
         bool hide = DespawnAnimation();
		 if(hide)
		 {
			 //print("Im disabled");
			 this.GetComponent<Collider>().enabled = false;
			 this.GetComponent<MeshRenderer>().renderingLayerMask = 0;
			 deathTimer += Time.deltaTime;
			 if(deathTimer >= playerSpawnCooldown)
			 {
				 
				 m_Restore();
				 
			 }
		 }
	  }
	}
	void Fire(string bulletId)
	{
		GameObject go = (GameObject)Instantiate(Resources.Load(bulletID),bulletNozzle.transform.position,Quaternion.identity);
		go.GetComponent<BaseProjectile>().bullet_Init(bulletDamage, bulletNozzle.transform, targetToSnapTo,this.gameObject.layer, validTargets);
		
	}
	void CameraLerpToStart()
	{
       camera.GetComponent<Camera>().fieldOfView -= cameraLerpValue;
	   if(camera.GetComponent<Camera>().fieldOfView < cameraFOVStart)
	   {
		   camera.GetComponent<Camera>().fieldOfView = cameraFOVStart;
	   }
	}
	bool DespawnAnimation()
	{
		return true;
	}
	protected override void m_Respawn()
	{
		print("Respawning");
		disabled = true;
	}
	protected override void m_Restore()
	{
		 //this.transform.position = spawnPoint.transform.position;
		 deathTimer = 0;
		 disabled = false;
		 this.GetComponent<MeshRenderer>().renderingLayerMask = 1;
		 this.GetComponent<Collider>().enabled = true;
		transform.position = spawnPoint.transform.position;
		 transform.up = transform.position - targetToSnapTo.transform.position;
		 camera.transform.position = transform.position + (cameraoffset.x * transform.forward) + (cameraoffset.y * transform.up);
		 camera.transform.LookAt(transform.position + new Vector3(0,cameraoffset.z,0), transform.up);
		 camera.GetComponent<Camera>().fieldOfView = cameraFOVStart;
		m_currentHitpoints = m_hitPoints;

	}
	bool lerpFromTo()
	{
		dashLerpValue = (dashMaxSpeed - dashStartValue) / (dashWindUpDown / Time.deltaTime);
		cameraLerpValue = (cameraFOVMax - cameraFOVStart) / (dashWindUpDown / Time.deltaTime);
	  if(dashState == 1){
		  dashLerpStep += dashLerpValue; 
		  if(dashLerpStep >= dashMaxSpeed) {dashState =2;}
          camera.GetComponent<Camera>().fieldOfView += cameraLerpValue;
		  } 
	  else if(dashState == 2){ dashTimer+= Time.deltaTime; if(dashTimer >= (dashLengthTime-(2*dashWindUpDown))){dashState = 3;}}
	  else if(dashState == 3){
		    dashLerpStep -= dashLerpValue;
			camera.GetComponent<Camera>().fieldOfView -= cameraLerpValue;
		    if(dashLerpStep <= dashStartValue)
			{
				dashTimer = 0;
                return false;
			} 
			}
      transform.RotateAround(targetToSnapTo.transform.position, transform.right, dashLerpStep);
	  return true;
	}
	public void SetGamePad(int i)
	{
        Horizontal = "Joy" + i + "X";
		Vertical = "Joy" + i + "Y";
		Shoot = "Joy" + i + "Fire";
		Rotate = "Joy" + i + "Rotate";
		
	}
}
