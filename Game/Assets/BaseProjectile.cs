﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider))]
public class BaseProjectile : MonoBehaviour {
	[SerializeField]
	protected int  projectileSpeed;

	[SerializeField]
	//float fireSpeed;
	Transform targetToSnapTo;
	[SerializeField]
	float activeTime, deathTimer;
	//public float FireSpeed{ get{return fireSpeed;}} 

    [SerializeField]
	int[] validTargets;
	int m_damage;

	public void bullet_Init(int damage, Transform fireTransform, Transform targetToSnapTo,int thisLayer, int[] targetLayers)
	{
      		m_damage = damage;
		    this.transform.right = fireTransform.right;
			this.targetToSnapTo = targetToSnapTo;
			this.gameObject.layer = thisLayer;
			validTargets = targetLayers;
			
	}
	void OnTriggerEnter(Collider Collider)
	{
		//print(Collider.gameObject.layer);
	
		foreach(int i in validTargets)
		{
		if(Collider.gameObject.layer == i)
		{
			//print("Colliding");
		   if(Collider.gameObject.GetComponent<Destroyable>() != null)
		   {
	         Collider.gameObject.GetComponent<Destroyable>().Damage(m_damage);
			 OnHit();	
			 return;	   
		   }
           Collider.gameObject.GetComponent<Destroyable>().Damage(m_damage);
		   OnHit();
		}
		}
	}
	
	protected virtual void UpdatePosition()
	{
		 if(deathTimer >= activeTime)
		 {
			 Destroy(this.gameObject);
		 }
         transform.RotateAround(targetToSnapTo.transform.position, transform.right, projectileSpeed * Time.deltaTime);
		 deathTimer += Time.deltaTime;
	}
	protected virtual void OnHit()
	{
		Destroy(this.gameObject);
	}
}
