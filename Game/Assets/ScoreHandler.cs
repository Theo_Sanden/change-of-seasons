﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
    struct Zone
	{
		public float progressTowardsNext;
		public bool team; //true if red/ false if blue; 
	}
public class ScoreHandler : MonoBehaviour {
    [SerializeField]
	float zoneProgressionStepSize;
	[SerializeField]
	int pointsPerZone, updateFrequenzy, numberOfZones;
    Zone[] zones;
	int redScore;
	int blueScore;
	int RedScore{get{return redScore;}}
	int BlueScore{get{return blueScore;}}
	float timer;
	[SerializeField]
	Text redScoreUI,blueScoreUI;
	bool firstZoneActivated;
	
	void Start () {
		zones = new Zone[numberOfZones];
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;
		if(timer >= updateFrequenzy){timer = 0;}
		if(firstZoneActivated && timer == 0){updateScore();}
		
	}
	public void updateZoneProgress(int multiplier, int zone, bool team)
	{
		if(team != zones[zone].team){zones[zone].progressTowardsNext += zoneProgressionStepSize * multiplier;}
		else if(team == zones[zone].team && zones[zone].progressTowardsNext > 0){zones[zone].progressTowardsNext -= zoneProgressionStepSize* multiplier;}
		if(zones[zone].progressTowardsNext >= 1){zones[zone].team = team; zones[zone].progressTowardsNext = 0; firstZoneActivated = true;}
		if(zones[zone].progressTowardsNext < 0){zones[zone].progressTowardsNext = 0;}
	}
    private void updateScore()
	{
		foreach(Zone zone in zones)
		{
			if(zone.team)
			{
				if(zone.progressTowardsNext < 0.5)
				{
                     redScore += pointsPerZone;
					 blueScoreUI.GetComponent<Text>().text = "" + redScore;
				}
			}
			else if(!zone.team)
			{
				if(zone.progressTowardsNext < 0.5)
				{
					blueScore += pointsPerZone;
					blueScoreUI.GetComponent<Text>().text = "" + blueScore;
				}
			}
		}
	}

}
